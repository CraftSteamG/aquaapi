package de.waterdu.aquaapi.ui.api;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ClickType;

import java.util.Set;

/**
 * Interface for creating {@link AquaUI} pages.
 *
 * @author      Sam Plummer <sam@waterdu.de>
 * @version     1.0
 * @since       02/03/2019
 */
public interface IPage {
    /**
     * Returns a list of buttons to be shown on the page.
     * Buttons are instances of {@link Button}.
     *
     * @param player The user this page is being shown to.
     * @return List\<Button\> Returns all buttons on this page to be shown to the user.
     */
    Set<Button> getButtons(EntityPlayerMP player);

    /**
     * Called when a button on the page is pressed.
     *
     * @param player The user this page is being shown to.
     * @param index The index of the button pressed.
     * @param clickType The {@link ClickType} of the click.
     */
    void onButtonClick(EntityPlayerMP player, int index, ClickType clickType);

    /**
     * Called when the page is closed.
     *
     * @param player The user this page is being shown to.
     * @return boolean Whether to continue with the closure.
     */
    boolean onClose(EntityPlayerMP player);

    /**
     * Gets the number of rows in a page. 3 = small chest, 6 = large chest.
     *
     * @param player The user this page is being shown to.
     * @return int The number of rows of slots on this page.
     */
    int getRows(EntityPlayerMP player);

    /**
     * Gets the display name of the page, shown on the top.
     *
     * @param player The user this page is being shown to.
     * @return String The display name of this page.
     */
    String getDisplayName(EntityPlayerMP player);
}
