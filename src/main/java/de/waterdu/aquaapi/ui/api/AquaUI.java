package de.waterdu.aquaapi.ui.api;

import de.waterdu.aquaapi.ui.internal.AquaUIContainer;
import de.waterdu.aquaapi.ui.internal.AquaUIInventory;
import de.waterdu.aquaapi.ui.internal.AquaUIListener;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketOpenWindow;
import net.minecraftforge.common.MinecraftForge;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.UUID;

/**
 * <h1>AquaUI</h1>
 * Chest UIs made in Forge directly.
 *
 * @author      Sam Plummer <sam@waterdu.de>
 * @version     1.0
 * @since       02/03/2019
 */
public class AquaUI {
    /**
     * Stores pages that should be opened in the future, once the player closes their current screen.
     */
    public static HashMap<UUID, IPage> futureOpen = new HashMap<>();

    /**
     * Registers AquaUI. Needed in order to force pages open, overriding ESC.
     * Should be called during FMLPreInitializationEvent.
     */
    public static void register() {
        MinecraftForge.EVENT_BUS.register(new AquaUIListener());
    }

    /**
     * Opens a UI page. All UI pages must be instances of a class implementing {@link IPage}.
     *
     * @param player The user to display the page to.
     * @param page The page to display. null closes any currently open page.
     * @return boolean True indicates success in showing page.
     */
    public static boolean openUI(EntityPlayerMP player, @Nullable IPage page) {
        return AquaUI.openUI(player, page, false);
    }

    /**
     * Opens a UI page. All UI pages must be instances of a class implementing {@link IPage}.
     *
     * @param player The user to display the page to.
     * @param page The page to display. null closes any currently open page.
     * @param waitForScreen Whether to wait for the player's current screen to close before opening the page.
     * @return boolean True indicates success in showing page.
     */
    public static boolean openUI(EntityPlayerMP player, @Nullable IPage page, boolean waitForScreen) {
        boolean result = false;
        if(player != null) {
            result = true;
            if(waitForScreen) {
                AquaUI.futureOpen.put(player.getPersistentID(), page);
            } else {
                if (page != null) {
                    player.closeContainer();
                    AquaUIInventory i = new AquaUIInventory(page, player);
                    AquaUIContainer c = new AquaUIContainer(player.inventory, i, player);
                    player.sendAllWindowProperties(c, i);
                    player.openContainer = c;
                    player.currentWindowId = 1;
                    player.connection.sendPacket(new SPacketOpenWindow(player.currentWindowId, "minecraft:container", i.getDisplayName(), page.getRows(player) * 9));
                    c.detectAndSendChanges();
                    i.markDirty();
                    for (Button b : i.buttons) {
                        player.sendSlotContents(c, b.getIndex(), b.getDisplay());
                        i.inventory.set(b.getIndex(), b.getDisplay());
                    }
                    player.sendAllContents(c, i.inventory);
                } else {
                    AquaUI.futureOpen.put(player.getPersistentID(), null);
                }
            }
        }
        return result;
    }
}