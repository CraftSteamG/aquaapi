package de.waterdu.aquaapi.ui.internal;

import de.waterdu.aquaapi.ui.api.Button;
import de.waterdu.aquaapi.ui.api.AquaUI;
import de.waterdu.aquaapi.ui.api.IPage;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

import java.util.Set;

public class AquaUIInventory implements IInventory {
    public IPage page;
    public EntityPlayerMP player;
    public Set<Button> buttons;
    public NonNullList<ItemStack> inventory;

    public AquaUIInventory(IPage page, EntityPlayerMP player) {
        this.inventory = NonNullList.withSize(page.getRows(player) * 9, ItemStack.EMPTY);
        this.page = page;
        this.player = player;
        this.buttons = page.getButtons(this.player);
    }

    @Override
    public int getSizeInventory() {
        return this.page.getRows(player) * 9;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return this.inventory.get(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        AquaUI.openUI(this.player, this.page);
        return ItemStack.EMPTY;
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        AquaUI.openUI(this.player, this.page);
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        AquaUI.openUI(this.player, this.page);
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public void markDirty() {

    }

    @Override
    public boolean isUsableByPlayer(EntityPlayer player) {
        return false;
    }

    @Override
    public void openInventory(EntityPlayer player) {

    }

    @Override
    public void closeInventory(EntityPlayer player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return false;
    }

    @Override
    public int getField(int id) {
        return 0;
    }

    @Override
    public void setField(int id, int value) {

    }

    @Override
    public int getFieldCount() {
        return 0;
    }

    @Override
    public void clear() {

    }

    @Override
    public String getName() {
        return this.page.getDisplayName(this.player);
    }

    @Override
    public boolean hasCustomName() {
        return true;
    }

    @Override
    public ITextComponent getDisplayName() {
        return new TextComponentString(this.page.getDisplayName(this.player));
    }
}