package de.waterdu.aquaapi.ui.internal;

import de.waterdu.aquaapi.ui.api.AquaUI;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class AquaUIContainer extends Container {
    private final AquaUIInventory inventory;

    public AquaUIContainer(IInventory playerInventory, AquaUIInventory inventory, EntityPlayer player) {
        this.inventory = inventory;
        int numRows = inventory.getSizeInventory() / 9;

        inventory.openInventory(player);
        int i = (numRows - 4) * 18;

        for (int j = 0; j < numRows; ++j)
        {
            for (int k = 0; k < 9; ++k)
            {
                this.addSlotToContainer(new Slot(inventory, k + j * 9, 8 + k * 18, 18 + j * 18));
            }
        }

        for (int l = 0; l < 3; ++l) {
            for (int j1 = 0; j1 < 9; ++j1) {
                this.addSlotToContainer(new Slot(playerInventory, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + i));
            }
        }

        for (int i1 = 0; i1 < 9; ++i1) {
            this.addSlotToContainer(new Slot(playerInventory, i1, 8 + i1 * 18, 161 + i));
        }

        this.windowId = 1;
    }

    public AquaUIInventory getAquaUIInventory() {
        return inventory;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int index) {
        AquaUI.openUI(this.inventory.player, this.inventory.page);
        return ItemStack.EMPTY;
    }

    @Override
    public ItemStack slotClick(int slot, int dragType, ClickType clickTypeIn, EntityPlayer player) {
        AquaUI.openUI(this.inventory.player, this.inventory.page);
        this.inventory.page.onButtonClick(this.inventory.player, slot, clickTypeIn);
        return ItemStack.EMPTY;
    }

    @Override
    protected boolean mergeItemStack(ItemStack stack, int start, int end, boolean backwards) {
        AquaUI.openUI(this.inventory.player, this.inventory.page);
        return false;
    }

    @Override
    public boolean canMergeSlot(ItemStack stack, Slot slotIn)
    {
        return false;
    }

    @Override
    public void onContainerClosed(EntityPlayer playerIn) {
        if(playerIn instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) playerIn;
            super.onContainerClosed(playerIn);
            this.inventory.closeInventory(playerIn);
            player.sendAllWindowProperties(player.inventoryContainer, player.inventory);
            player.sendContainerToPlayer(player.inventoryContainer);
            if(!this.inventory.page.onClose(player)) {
                AquaUI.openUI(player, this.inventory.page, true);
            }
        }
    }

    @Override
    public void putStackInSlot(int slotID, ItemStack stack) {

    }

    @Override
    public boolean canDragIntoSlot(Slot slotIn) {
        return false;
    }
}