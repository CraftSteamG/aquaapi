package de.waterdu.aquaapi.ui.internal;

import de.waterdu.aquaapi.ui.api.AquaUI;
import de.waterdu.aquaapi.ui.api.IPage;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.client.CPacketCloseWindow;
import net.minecraft.network.play.server.SPacketCloseWindow;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class AquaUIListener {
    @SubscribeEvent
    public void onTick(TickEvent.ServerTickEvent event) {
        MinecraftServer server = FMLCommonHandler.instance().getMinecraftServerInstance();
        for(Iterator<Map.Entry<UUID, IPage>> iterator = AquaUI.futureOpen.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<UUID, IPage> entry = iterator.next();
            UUID uuid = entry.getKey();
            IPage page = entry.getValue();
            EntityPlayerMP player = server.getPlayerList().getPlayerByUUID(uuid);
            if(page == null) {
                CPacketCloseWindow pclient = new CPacketCloseWindow();
                ObfuscationReflectionHelper.setPrivateValue(CPacketCloseWindow.class, pclient, player.openContainer.windowId, 0);
                SPacketCloseWindow pserver = new SPacketCloseWindow(player.openContainer.windowId);
                player.connection.processCloseWindow(pclient);
                player.connection.sendPacket(pserver);
                iterator.remove();
                continue;
            }
            if(player == null) {
                continue;
            }
            if(player.openContainer == player.inventoryContainer) {
                AquaUI.openUI(player, page);
                iterator.remove();
            }
        }
    }
}
